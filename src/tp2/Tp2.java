/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp2;

import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;
import net.tinyos.message.MoteIF;

public class Tp2 {

    public static void main(String[] args) {
        MainPanel main = new MainPanel();
        MoteIF mote = main.getMote();

        JFrame mainWindow = new JFrame("TP2");
        mainWindow.setMinimumSize(new Dimension(800, 500));
        mainWindow.setContentPane(main);
        mainWindow.setLocationRelativeTo(null);
        mainWindow.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        mainWindow.setVisible(true);
        
        mainWindow.addWindowListener(new WindowAdapter() {
          @Override
          public void windowClosed(WindowEvent evt) {
            main.disableTimer();
            mote.getSource().shutdown();
          }
        });
    }
}
