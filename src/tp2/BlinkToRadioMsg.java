/**
 * This class is automatically generated by mig. DO NOT EDIT THIS FILE.
 * This class implements a Java interface to the 'BlinkToRadioMsg'
 * message type.
 */

package tp2;

public class BlinkToRadioMsg extends net.tinyos.message.Message {

    /** The default size of this message type in bytes. */
    public static final int DEFAULT_MESSAGE_SIZE = 28;

    /** The Active Message type associated with this message. */
    public static final int AM_TYPE = 6;

    /** Create a new BlinkToRadioMsg of size 28. */
    public BlinkToRadioMsg() {
        super(DEFAULT_MESSAGE_SIZE);
        amTypeSet(AM_TYPE);
    }

    /** Create a new BlinkToRadioMsg of the given data_length. */
    public BlinkToRadioMsg(int data_length) {
        super(data_length);
        amTypeSet(AM_TYPE);
    }

    /**
     * Create a new BlinkToRadioMsg with the given data_length
     * and base offset.
     */
    public BlinkToRadioMsg(int data_length, int base_offset) {
        super(data_length, base_offset);
        amTypeSet(AM_TYPE);
    }

    /**
     * Create a new BlinkToRadioMsg using the given byte array
     * as backing store.
     */
    public BlinkToRadioMsg(byte[] data) {
        super(data);
        amTypeSet(AM_TYPE);
    }

    /**
     * Create a new BlinkToRadioMsg using the given byte array
     * as backing store, with the given base offset.
     */
    public BlinkToRadioMsg(byte[] data, int base_offset) {
        super(data, base_offset);
        amTypeSet(AM_TYPE);
    }

    /**
     * Create a new BlinkToRadioMsg using the given byte array
     * as backing store, with the given base offset and data length.
     */
    public BlinkToRadioMsg(byte[] data, int base_offset, int data_length) {
        super(data, base_offset, data_length);
        amTypeSet(AM_TYPE);
    }

    /**
     * Create a new BlinkToRadioMsg embedded in the given message
     * at the given base offset.
     */
    public BlinkToRadioMsg(net.tinyos.message.Message msg, int base_offset) {
        super(msg, base_offset, DEFAULT_MESSAGE_SIZE);
        amTypeSet(AM_TYPE);
    }

    /**
     * Create a new BlinkToRadioMsg embedded in the given message
     * at the given base offset and length.
     */
    public BlinkToRadioMsg(net.tinyos.message.Message msg, int base_offset, int data_length) {
        super(msg, base_offset, data_length);
        amTypeSet(AM_TYPE);
    }

    /**
    /* Return a String representation of this message. Includes the
     * message type name and the non-indexed field values.
     */
    public String toString() {
      String s = "Message <BlinkToRadioMsg> \n";
      try {
        s += "  [src_address=0x"+Long.toHexString(get_src_address())+"]\n";
      } catch (ArrayIndexOutOfBoundsException aioobe) { /* Skip field */ }
      try {
        s += "  [dst_address=0x"+Long.toHexString(get_dst_address())+"]\n";
      } catch (ArrayIndexOutOfBoundsException aioobe) { /* Skip field */ }
      try {
        s += "  [type=0x"+Long.toHexString(get_type())+"]\n";
      } catch (ArrayIndexOutOfBoundsException aioobe) { /* Skip field */ }
      try {
        s += "  [length=0x"+Long.toHexString(get_length())+"]\n";
      } catch (ArrayIndexOutOfBoundsException aioobe) { /* Skip field */ }
      try {
        s += "  [hops=0x"+Long.toHexString(get_hops())+"]\n";
      } catch (ArrayIndexOutOfBoundsException aioobe) { /* Skip field */ }
      try {
        s += "  [flood_id=0x"+Long.toHexString(get_flood_id())+"]\n";
      } catch (ArrayIndexOutOfBoundsException aioobe) { /* Skip field */ }
      try {
        s += "  [father_id=0x"+Long.toHexString(get_father_id())+"]\n";
      } catch (ArrayIndexOutOfBoundsException aioobe) { /* Skip field */ }
      try {
        s += "  [temperature=0x"+Long.toHexString(get_temperature())+"]\n";
      } catch (ArrayIndexOutOfBoundsException aioobe) { /* Skip field */ }
      try {
        s += "  [luminosity=0x"+Long.toHexString(get_luminosity())+"]\n";
      } catch (ArrayIndexOutOfBoundsException aioobe) { /* Skip field */ }
      try {
        s += "  [next_id=0x"+Long.toHexString(get_next_id())+"]\n";
      } catch (ArrayIndexOutOfBoundsException aioobe) { /* Skip field */ }
      try {
        s += "  [unused=";
        for (int i = 0; i < 13; i++) {
          s += "0x"+Long.toHexString(getElement_unused(i) & 0xff)+" ";
        }
        s += "]\n";
      } catch (ArrayIndexOutOfBoundsException aioobe) { /* Skip field */ }
      return s;
    }

    // Message-type-specific access methods appear below.

    /////////////////////////////////////////////////////////
    // Accessor methods for field: src_address
    //   Field type: int, unsigned
    //   Offset (bits): 0
    //   Size (bits): 16
    /////////////////////////////////////////////////////////

    /**
     * Return whether the field 'src_address' is signed (false).
     */
    public static boolean isSigned_src_address() {
        return false;
    }

    /**
     * Return whether the field 'src_address' is an array (false).
     */
    public static boolean isArray_src_address() {
        return false;
    }

    /**
     * Return the offset (in bytes) of the field 'src_address'
     */
    public static int offset_src_address() {
        return (0 / 8);
    }

    /**
     * Return the offset (in bits) of the field 'src_address'
     */
    public static int offsetBits_src_address() {
        return 0;
    }

    /**
     * Return the value (as a int) of the field 'src_address'
     */
    public int get_src_address() {
        return (int)getUIntBEElement(offsetBits_src_address(), 16);
    }

    /**
     * Set the value of the field 'src_address'
     */
    public void set_src_address(int value) {
        setUIntBEElement(offsetBits_src_address(), 16, value);
    }

    /**
     * Return the size, in bytes, of the field 'src_address'
     */
    public static int size_src_address() {
        return (16 / 8);
    }

    /**
     * Return the size, in bits, of the field 'src_address'
     */
    public static int sizeBits_src_address() {
        return 16;
    }

    /////////////////////////////////////////////////////////
    // Accessor methods for field: dst_address
    //   Field type: int, unsigned
    //   Offset (bits): 16
    //   Size (bits): 16
    /////////////////////////////////////////////////////////

    /**
     * Return whether the field 'dst_address' is signed (false).
     */
    public static boolean isSigned_dst_address() {
        return false;
    }

    /**
     * Return whether the field 'dst_address' is an array (false).
     */
    public static boolean isArray_dst_address() {
        return false;
    }

    /**
     * Return the offset (in bytes) of the field 'dst_address'
     */
    public static int offset_dst_address() {
        return (16 / 8);
    }

    /**
     * Return the offset (in bits) of the field 'dst_address'
     */
    public static int offsetBits_dst_address() {
        return 16;
    }

    /**
     * Return the value (as a int) of the field 'dst_address'
     */
    public int get_dst_address() {
        return (int)getUIntBEElement(offsetBits_dst_address(), 16);
    }

    /**
     * Set the value of the field 'dst_address'
     */
    public void set_dst_address(int value) {
        setUIntBEElement(offsetBits_dst_address(), 16, value);
    }

    /**
     * Return the size, in bytes, of the field 'dst_address'
     */
    public static int size_dst_address() {
        return (16 / 8);
    }

    /**
     * Return the size, in bits, of the field 'dst_address'
     */
    public static int sizeBits_dst_address() {
        return 16;
    }

    /////////////////////////////////////////////////////////
    // Accessor methods for field: type
    //   Field type: short, unsigned
    //   Offset (bits): 32
    //   Size (bits): 8
    /////////////////////////////////////////////////////////

    /**
     * Return whether the field 'type' is signed (false).
     */
    public static boolean isSigned_type() {
        return false;
    }

    /**
     * Return whether the field 'type' is an array (false).
     */
    public static boolean isArray_type() {
        return false;
    }

    /**
     * Return the offset (in bytes) of the field 'type'
     */
    public static int offset_type() {
        return (32 / 8);
    }

    /**
     * Return the offset (in bits) of the field 'type'
     */
    public static int offsetBits_type() {
        return 32;
    }

    /**
     * Return the value (as a short) of the field 'type'
     */
    public short get_type() {
        return (short)getUIntBEElement(offsetBits_type(), 8);
    }

    /**
     * Set the value of the field 'type'
     */
    public void set_type(short value) {
        setUIntBEElement(offsetBits_type(), 8, value);
    }

    /**
     * Return the size, in bytes, of the field 'type'
     */
    public static int size_type() {
        return (8 / 8);
    }

    /**
     * Return the size, in bits, of the field 'type'
     */
    public static int sizeBits_type() {
        return 8;
    }

    /////////////////////////////////////////////////////////
    // Accessor methods for field: length
    //   Field type: short, unsigned
    //   Offset (bits): 40
    //   Size (bits): 8
    /////////////////////////////////////////////////////////

    /**
     * Return whether the field 'length' is signed (false).
     */
    public static boolean isSigned_length() {
        return false;
    }

    /**
     * Return whether the field 'length' is an array (false).
     */
    public static boolean isArray_length() {
        return false;
    }

    /**
     * Return the offset (in bytes) of the field 'length'
     */
    public static int offset_length() {
        return (40 / 8);
    }

    /**
     * Return the offset (in bits) of the field 'length'
     */
    public static int offsetBits_length() {
        return 40;
    }

    /**
     * Return the value (as a short) of the field 'length'
     */
    public short get_length() {
        return (short)getUIntBEElement(offsetBits_length(), 8);
    }

    /**
     * Set the value of the field 'length'
     */
    public void set_length(short value) {
        setUIntBEElement(offsetBits_length(), 8, value);
    }

    /**
     * Return the size, in bytes, of the field 'length'
     */
    public static int size_length() {
        return (8 / 8);
    }

    /**
     * Return the size, in bits, of the field 'length'
     */
    public static int sizeBits_length() {
        return 8;
    }

    /////////////////////////////////////////////////////////
    // Accessor methods for field: hops
    //   Field type: short, unsigned
    //   Offset (bits): 48
    //   Size (bits): 8
    /////////////////////////////////////////////////////////

    /**
     * Return whether the field 'hops' is signed (false).
     */
    public static boolean isSigned_hops() {
        return false;
    }

    /**
     * Return whether the field 'hops' is an array (false).
     */
    public static boolean isArray_hops() {
        return false;
    }

    /**
     * Return the offset (in bytes) of the field 'hops'
     */
    public static int offset_hops() {
        return (48 / 8);
    }

    /**
     * Return the offset (in bits) of the field 'hops'
     */
    public static int offsetBits_hops() {
        return 48;
    }

    /**
     * Return the value (as a short) of the field 'hops'
     */
    public short get_hops() {
        return (short)getUIntBEElement(offsetBits_hops(), 8);
    }

    /**
     * Set the value of the field 'hops'
     */
    public void set_hops(short value) {
        setUIntBEElement(offsetBits_hops(), 8, value);
    }

    /**
     * Return the size, in bytes, of the field 'hops'
     */
    public static int size_hops() {
        return (8 / 8);
    }

    /**
     * Return the size, in bits, of the field 'hops'
     */
    public static int sizeBits_hops() {
        return 8;
    }

    /////////////////////////////////////////////////////////
    // Accessor methods for field: flood_id
    //   Field type: short, unsigned
    //   Offset (bits): 56
    //   Size (bits): 8
    /////////////////////////////////////////////////////////

    /**
     * Return whether the field 'flood_id' is signed (false).
     */
    public static boolean isSigned_flood_id() {
        return false;
    }

    /**
     * Return whether the field 'flood_id' is an array (false).
     */
    public static boolean isArray_flood_id() {
        return false;
    }

    /**
     * Return the offset (in bytes) of the field 'flood_id'
     */
    public static int offset_flood_id() {
        return (56 / 8);
    }

    /**
     * Return the offset (in bits) of the field 'flood_id'
     */
    public static int offsetBits_flood_id() {
        return 56;
    }

    /**
     * Return the value (as a short) of the field 'flood_id'
     */
    public short get_flood_id() {
        return (short)getUIntBEElement(offsetBits_flood_id(), 8);
    }

    /**
     * Set the value of the field 'flood_id'
     */
    public void set_flood_id(short value) {
        setUIntBEElement(offsetBits_flood_id(), 8, value);
    }

    /**
     * Return the size, in bytes, of the field 'flood_id'
     */
    public static int size_flood_id() {
        return (8 / 8);
    }

    /**
     * Return the size, in bits, of the field 'flood_id'
     */
    public static int sizeBits_flood_id() {
        return 8;
    }

    /////////////////////////////////////////////////////////
    // Accessor methods for field: father_id
    //   Field type: int, unsigned
    //   Offset (bits): 64
    //   Size (bits): 16
    /////////////////////////////////////////////////////////

    /**
     * Return whether the field 'father_id' is signed (false).
     */
    public static boolean isSigned_father_id() {
        return false;
    }

    /**
     * Return whether the field 'father_id' is an array (false).
     */
    public static boolean isArray_father_id() {
        return false;
    }

    /**
     * Return the offset (in bytes) of the field 'father_id'
     */
    public static int offset_father_id() {
        return (64 / 8);
    }

    /**
     * Return the offset (in bits) of the field 'father_id'
     */
    public static int offsetBits_father_id() {
        return 64;
    }

    /**
     * Return the value (as a int) of the field 'father_id'
     */
    public int get_father_id() {
        return (int)getUIntBEElement(offsetBits_father_id(), 16);
    }

    /**
     * Set the value of the field 'father_id'
     */
    public void set_father_id(int value) {
        setUIntBEElement(offsetBits_father_id(), 16, value);
    }

    /**
     * Return the size, in bytes, of the field 'father_id'
     */
    public static int size_father_id() {
        return (16 / 8);
    }

    /**
     * Return the size, in bits, of the field 'father_id'
     */
    public static int sizeBits_father_id() {
        return 16;
    }

    /////////////////////////////////////////////////////////
    // Accessor methods for field: temperature
    //   Field type: int, unsigned
    //   Offset (bits): 80
    //   Size (bits): 16
    /////////////////////////////////////////////////////////

    /**
     * Return whether the field 'temperature' is signed (false).
     */
    public static boolean isSigned_temperature() {
        return false;
    }

    /**
     * Return whether the field 'temperature' is an array (false).
     */
    public static boolean isArray_temperature() {
        return false;
    }

    /**
     * Return the offset (in bytes) of the field 'temperature'
     */
    public static int offset_temperature() {
        return (80 / 8);
    }

    /**
     * Return the offset (in bits) of the field 'temperature'
     */
    public static int offsetBits_temperature() {
        return 80;
    }

    /**
     * Return the value (as a int) of the field 'temperature'
     */
    public int get_temperature() {
        return (int)getUIntBEElement(offsetBits_temperature(), 16);
    }

    /**
     * Set the value of the field 'temperature'
     */
    public void set_temperature(int value) {
        setUIntBEElement(offsetBits_temperature(), 16, value);
    }

    /**
     * Return the size, in bytes, of the field 'temperature'
     */
    public static int size_temperature() {
        return (16 / 8);
    }

    /**
     * Return the size, in bits, of the field 'temperature'
     */
    public static int sizeBits_temperature() {
        return 16;
    }

    /////////////////////////////////////////////////////////
    // Accessor methods for field: luminosity
    //   Field type: int, unsigned
    //   Offset (bits): 96
    //   Size (bits): 16
    /////////////////////////////////////////////////////////

    /**
     * Return whether the field 'luminosity' is signed (false).
     */
    public static boolean isSigned_luminosity() {
        return false;
    }

    /**
     * Return whether the field 'luminosity' is an array (false).
     */
    public static boolean isArray_luminosity() {
        return false;
    }

    /**
     * Return the offset (in bytes) of the field 'luminosity'
     */
    public static int offset_luminosity() {
        return (96 / 8);
    }

    /**
     * Return the offset (in bits) of the field 'luminosity'
     */
    public static int offsetBits_luminosity() {
        return 96;
    }

    /**
     * Return the value (as a int) of the field 'luminosity'
     */
    public int get_luminosity() {
        return (int)getUIntBEElement(offsetBits_luminosity(), 16);
    }

    /**
     * Set the value of the field 'luminosity'
     */
    public void set_luminosity(int value) {
        setUIntBEElement(offsetBits_luminosity(), 16, value);
    }

    /**
     * Return the size, in bytes, of the field 'luminosity'
     */
    public static int size_luminosity() {
        return (16 / 8);
    }

    /**
     * Return the size, in bits, of the field 'luminosity'
     */
    public static int sizeBits_luminosity() {
        return 16;
    }

    /////////////////////////////////////////////////////////
    // Accessor methods for field: next_id
    //   Field type: short, unsigned
    //   Offset (bits): 112
    //   Size (bits): 8
    /////////////////////////////////////////////////////////

    /**
     * Return whether the field 'next_id' is signed (false).
     */
    public static boolean isSigned_next_id() {
        return false;
    }

    /**
     * Return whether the field 'next_id' is an array (false).
     */
    public static boolean isArray_next_id() {
        return false;
    }

    /**
     * Return the offset (in bytes) of the field 'next_id'
     */
    public static int offset_next_id() {
        return (112 / 8);
    }

    /**
     * Return the offset (in bits) of the field 'next_id'
     */
    public static int offsetBits_next_id() {
        return 112;
    }

    /**
     * Return the value (as a short) of the field 'next_id'
     */
    public short get_next_id() {
        return (short)getUIntBEElement(offsetBits_next_id(), 8);
    }

    /**
     * Set the value of the field 'next_id'
     */
    public void set_next_id(short value) {
        setUIntBEElement(offsetBits_next_id(), 8, value);
    }

    /**
     * Return the size, in bytes, of the field 'next_id'
     */
    public static int size_next_id() {
        return (8 / 8);
    }

    /**
     * Return the size, in bits, of the field 'next_id'
     */
    public static int sizeBits_next_id() {
        return 8;
    }

    /////////////////////////////////////////////////////////
    // Accessor methods for field: unused
    //   Field type: short[], unsigned
    //   Offset (bits): 120
    //   Size of each element (bits): 8
    /////////////////////////////////////////////////////////

    /**
     * Return whether the field 'unused' is signed (false).
     */
    public static boolean isSigned_unused() {
        return false;
    }

    /**
     * Return whether the field 'unused' is an array (true).
     */
    public static boolean isArray_unused() {
        return true;
    }

    /**
     * Return the offset (in bytes) of the field 'unused'
     */
    public static int offset_unused(int index1) {
        int offset = 120;
        if (index1 < 0 || index1 >= 13) throw new ArrayIndexOutOfBoundsException();
        offset += 0 + index1 * 8;
        return (offset / 8);
    }

    /**
     * Return the offset (in bits) of the field 'unused'
     */
    public static int offsetBits_unused(int index1) {
        int offset = 120;
        if (index1 < 0 || index1 >= 13) throw new ArrayIndexOutOfBoundsException();
        offset += 0 + index1 * 8;
        return offset;
    }

    /**
     * Return the entire array 'unused' as a short[]
     */
    public short[] get_unused() {
        short[] tmp = new short[13];
        for (int index0 = 0; index0 < numElements_unused(0); index0++) {
            tmp[index0] = getElement_unused(index0);
        }
        return tmp;
    }

    /**
     * Set the contents of the array 'unused' from the given short[]
     */
    public void set_unused(short[] value) {
        for (int index0 = 0; index0 < value.length; index0++) {
            setElement_unused(index0, value[index0]);
        }
    }

    /**
     * Return an element (as a short) of the array 'unused'
     */
    public short getElement_unused(int index1) {
        return (short)getUIntBEElement(offsetBits_unused(index1), 8);
    }

    /**
     * Set an element of the array 'unused'
     */
    public void setElement_unused(int index1, short value) {
        setUIntBEElement(offsetBits_unused(index1), 8, value);
    }

    /**
     * Return the total size, in bytes, of the array 'unused'
     */
    public static int totalSize_unused() {
        return (104 / 8);
    }

    /**
     * Return the total size, in bits, of the array 'unused'
     */
    public static int totalSizeBits_unused() {
        return 104;
    }

    /**
     * Return the size, in bytes, of each element of the array 'unused'
     */
    public static int elementSize_unused() {
        return (8 / 8);
    }

    /**
     * Return the size, in bits, of each element of the array 'unused'
     */
    public static int elementSizeBits_unused() {
        return 8;
    }

    /**
     * Return the number of dimensions in the array 'unused'
     */
    public static int numDimensions_unused() {
        return 1;
    }

    /**
     * Return the number of elements in the array 'unused'
     */
    public static int numElements_unused() {
        return 13;
    }

    /**
     * Return the number of elements in the array 'unused'
     * for the given dimension.
     */
    public static int numElements_unused(int dimension) {
      int array_dims[] = { 13,  };
        if (dimension < 0 || dimension >= 1) throw new ArrayIndexOutOfBoundsException();
        if (array_dims[dimension] == 0) throw new IllegalArgumentException("Array dimension "+dimension+" has unknown size");
        return array_dims[dimension];
    }

    /**
     * Fill in the array 'unused' with a String
     */
    public void setString_unused(String s) { 
         int len = s.length();
         int i;
         for (i = 0; i < len; i++) {
             setElement_unused(i, (short)s.charAt(i));
         }
         setElement_unused(i, (short)0); //null terminate
    }

    /**
     * Read the array 'unused' as a String
     */
    public String getString_unused() { 
         char carr[] = new char[Math.min(net.tinyos.message.Message.MAX_CONVERTED_STRING_LENGTH,13)];
         int i;
         for (i = 0; i < carr.length; i++) {
             if ((char)getElement_unused(i) == (char)0) break;
             carr[i] = (char)getElement_unused(i);
         }
         return new String(carr,0,i);
    }

}
