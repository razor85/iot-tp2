/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp2;

import com.github.jabbalaci.graphviz.GraphViz;
import java.awt.Color;
import java.awt.Dialog;
import java.awt.Dimension;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import net.tinyos.message.Message;
import net.tinyos.message.MoteIF;
import net.tinyos.packet.BuildSource;
import net.tinyos.util.PrintStreamMessenger;

/**
 * Main class panel.
 *
 * @author Romulo Fernnades <abra185@gmail.com>
 */
public class MainPanel extends javax.swing.JPanel implements net.tinyos.message.MessageListener {

  private int requestInterval = 2;
  private OpMode opMode;
  private Timer autoTimer;

  // Wait 0.5 second before drawing graph.
  private Timer drawGraphTimer;

  // Data collected
  private final List<SensorData> collectedData;

  // Radio settings.
  private final String SERVER_ADDR = "sf@192.168.201.136:9002";
  private final MoteIF moteIF;
  private short lastFloodID = 0;

  private enum OpMode {
    MANUAL,
    AUTO
  }

  /**
   * Creates new form MainPanel
   */
  public MainPanel() {
    initComponents();
    collectedData = new ArrayList<>();

    modeComboBox.addItemListener((itemEvt) -> {
      int selectedMode = modeComboBox.getSelectedIndex();
      if (selectedMode == 0) {
        opMode = OpMode.MANUAL;
      } else {
        opMode = OpMode.AUTO;
      }

      updateMode();
    });

    settingsButton.addActionListener((evt) -> {
      openSettings();
    });

    sendRequestButton.addActionListener((evt) -> {
      sendRequest();
    });

    graphScrollPane.setBackground(Color.WHITE);
    graphScrollPane.setOpaque(true);
    graphLabel.setBackground(Color.WHITE);
    graphLabel.setOpaque(true);

    moteIF = new MoteIF(BuildSource.makePhoenix(SERVER_ADDR,
        PrintStreamMessenger.out));

    BlinkToRadioMsg packet = new BlinkToRadioMsg();
    moteIF.registerListener(packet, this);

    drawGraph();
  }

  public MoteIF getMote() {
    return moteIF;
  }

  public void disableTimer() {
    if (autoTimer != null)
      autoTimer.stop();
  }

  private void updateMode() {
    sendRequestButton.setEnabled(opMode == OpMode.MANUAL);
    if (autoTimer != null) {
      autoTimer.stop();
    }

    if (opMode == OpMode.AUTO) {
      autoTimer = new Timer(requestInterval * 1000, (evt) -> {
        sendRequest();
      });

      autoTimer.start();
    }
  }

  private double readTemperature(int temp) {
    if (temp <= 0 || temp > 1024)
        return 0;

    final double rth = 10000.0 * (double)(1023 - temp) / (double)temp;
    final double a = 0.001010024;
    final double b = 0.000242127;
    final double c = 0.000000146;

    final double kelvin = 1.0 / (a + 
        b * Math.log(rth) + 
        c * Math.pow(Math.log(rth), 3.0));

    final double celsius = kelvin - 273.15;
    return BigDecimal.valueOf(celsius)
        .setScale(1, RoundingMode.HALF_UP)
        .doubleValue();
  }

  private double readLuminosity(int lumen) {
    if (lumen <= 0 || lumen > 1024)
        return 0;

    double normalized = (double) lumen / 1024.0;
    return BigDecimal.valueOf(normalized * 100.0)
        .setScale(0, RoundingMode.HALF_UP)
        .doubleValue();
  }

  @Override
  public void messageReceived(int i, Message msg) {
    BlinkToRadioMsg packet = (BlinkToRadioMsg) msg;
    System.out.println("Received " + packet.get_type() + " from " + 
      packet.get_src_address() + "(" + packet.get_flood_id() + ")");

    if (packet.get_type() == 0x93 ) {
      // Search for this packet.
      SensorData newData = null;
      for (SensorData sensor : collectedData) {
        if (sensor.id == packet.get_src_address()) {
          newData = sensor;
          collectedData.remove(sensor);

          break;
        }
      }

      if (newData == null) {
        newData = new SensorData();
      }

      newData.id = packet.get_src_address();
      newData.fatherID = packet.get_father_id();
      newData.floodID = packet.get_flood_id();
      newData.luminosity = readLuminosity(packet.get_luminosity());
      newData.temperature = readTemperature(packet.get_temperature());

      collectedData.add(newData);
      Collections.sort(collectedData, (SensorData t, SensorData t1) -> {
        return Integer.compare(t.id, t1.id);
      });

      double waitTime = (requestInterval * 1000) / 4;
      setDrawGraphTimer((int)waitTime);
    }
  }

  private void setDrawGraphTimer(int timeout) {
    // Wait 0.5s before drawing.
    if (drawGraphTimer != null)
      drawGraphTimer.stop();
    
    drawGraphTimer = new Timer(timeout, (evt) -> {
      drawGraph();
      drawGraphTimer.stop();
    });
    
    drawGraphTimer.start();
  }

  private void openSettings() {
    JDialog settings = new JDialog(SwingUtilities.getWindowAncestor(this),
        "Settings", Dialog.ModalityType.APPLICATION_MODAL);

    SettingsPanel panel = new SettingsPanel(requestInterval);
    panel.getCancel().addActionListener((evt) -> {
      settings.dispose();
    });

    panel.getOk().addActionListener((evt) -> {
      String interval = panel.getRequestInterval().getText();
      try {
        int newInterval = Integer.parseInt(interval);
        if (newInterval <= 1) {
          JOptionPane.showMessageDialog(this,
            "Interval must be greater than 1.");

          return;
        }

        requestInterval = newInterval;
        settings.dispose();
      } catch (NumberFormatException ex) {
        JOptionPane.showMessageDialog(settings,
            "Please specify a valid number (in seconds).");
      }
    });

    settings.setMinimumSize(new Dimension(300, 200));
    settings.setLocationRelativeTo(SwingUtilities.getWindowAncestor(this));
    settings.setContentPane(panel);
    settings.setVisible(true);
  }

  private void sendRequest() {
    try {
      sendRequestImpl();
    } catch (Exception ex) {
      JOptionPane.showMessageDialog(this,
          "Error sending request: " + ex.getMessage());
    }
  }

  private void sendRequestImpl()
      throws Exception {
      
    // Reset collected data and update sensors drawing.
    collectedData.clear();
    collectedData.add(new SensorData());

    byte[] blankData = new byte[28];
    for (int i = 0; i < 28; ++i)
        blankData[i] = 0;

    BlinkToRadioMsg packet = new BlinkToRadioMsg(blankData);
    packet.set_type((byte) 0x42);
    packet.set_flood_id(lastFloodID++);
    moteIF.send(MoteIF.TOS_BCAST_ADDR, packet);

    JFrame parentWindow = (JFrame)SwingUtilities.getWindowAncestor(this);
    parentWindow.setTitle("TP2 - Flood ID: " + (lastFloodID - 1));

    // Wrap
    if (lastFloodID > 255)
      lastFloodID = 0;
  }

  private void drawGraph() {
    GraphViz gv = new GraphViz("./graphviz/bin/dot.exe", "./temp");
    gv.addln(gv.start_graph());

    // Labels
    gv.addln("0 [label=\"Base\"];");
    for (SensorData sensor : collectedData) {
      if (sensor.id == 0) {
        continue;
      }

      StringBuilder line = new StringBuilder();
      line.append(Integer.toString(sensor.id));
      line.append(" [shape=box label=\"ID: ");
      line.append(Integer.toString(sensor.id));
      line.append("\nFloodID: ");
      line.append(Integer.toString(sensor.floodID));
      line.append("\nTemperature: ");
      line.append(Double.toString(sensor.temperature));
      line.append("º\nLuminosity: ");
      line.append(Double.toString(sensor.luminosity));
      line.append("%\"];");
      gv.addln(line.toString());
    }

    for (SensorData sensor : collectedData) {

      // Ignore base connection.
      if (sensor.id == 0) {
        continue;
      }

      boolean fatherFound = false;
      for (SensorData sensor2 : collectedData) {
          if (sensor2.id == sensor.id)
              continue;

          if (sensor2.id == sensor.fatherID) {
              fatherFound = true;
              break;
          }
      }

      if (fatherFound == false)
          continue;

      StringBuilder line = new StringBuilder();
      line.append(Integer.toString(sensor.id));
      line.append(" -> ");
      line.append(Integer.toString(sensor.fatherID));
      gv.addln(line.toString());
    }

    gv.addln(gv.end_graph());
    gv.increaseDpi();   // 106 dpi

    String type = "png";
    String repesentationType = "dot";

    byte[] graph = gv.getGraph(gv.getDotSource(), type, repesentationType);
    assert (graph != null);

    ImageIcon newIcon = new ImageIcon(graph);
    graphLabel.setIcon(newIcon);

    graphScrollPane.invalidate();
    graphScrollPane.revalidate();
    graphScrollPane.repaint();
  }

  /**
   * This method is called from within the constructor to initialize the form.
   * WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanel1 = new javax.swing.JPanel();
        modeComboBox = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();
        settingsButton = new javax.swing.JButton();
        graphScrollPane = new javax.swing.JScrollPane();
        jPanel2 = new javax.swing.JPanel();
        graphLabel = new javax.swing.JLabel();
        sendRequestButton = new javax.swing.JButton();

        setLayout(new java.awt.GridBagLayout());

        jPanel1.setLayout(new java.awt.GridBagLayout());

        modeComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Manual", "Automatic" }));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(4, 4, 4, 4);
        jPanel1.add(modeComboBox, gridBagConstraints);

        jLabel1.setText("Mode:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        gridBagConstraints.insets = new java.awt.Insets(4, 4, 4, 4);
        jPanel1.add(jLabel1, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        add(jPanel1, gridBagConstraints);

        settingsButton.setText("Settings");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(4, 4, 4, 4);
        add(settingsButton, gridBagConstraints);

        graphScrollPane.setBackground(new java.awt.Color(255, 255, 255));

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setLayout(new java.awt.GridBagLayout());

        graphLabel.setBackground(new java.awt.Color(255, 255, 255));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanel2.add(graphLabel, gridBagConstraints);

        graphScrollPane.setViewportView(jPanel2);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(4, 4, 4, 4);
        add(graphScrollPane, gridBagConstraints);

        sendRequestButton.setText("Send Request");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.insets = new java.awt.Insets(4, 4, 4, 4);
        add(sendRequestButton, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel graphLabel;
    private javax.swing.JScrollPane graphScrollPane;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JComboBox<String> modeComboBox;
    private javax.swing.JButton sendRequestButton;
    private javax.swing.JButton settingsButton;
    // End of variables declaration//GEN-END:variables
}
