// $Id: BlinkToRadio.h,v 1.4 2006/12/12 18:22:52 vlahan Exp $

#ifndef BLINKTORADIO_H
#define BLINKTORADIO_H

enum {
  AM_BLINKTORADIOMSG = 6
};

typedef nx_struct BlinkToRadioMsg {
  nx_uint16_t src_address;
  nx_uint16_t dst_address;
  nx_uint8_t type;
  nx_uint8_t length;
  nx_uint8_t hops;
  nx_uint8_t flood_id;
  nx_uint16_t father_id;
  nx_uint16_t temperature;  
  nx_uint16_t luminosity;
  nx_uint8_t next_id;  
  nx_uint8_t unused[13];
} BlinkToRadioMsg;

#endif
